﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public Tilemap myMap;
    private Vector3 bottomLeftLimit;
    private Vector3 topRightLimit;

    private float halfHeight;
    private float halfWidth;

    public int musicToPlay;
    private bool musicStarted;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerController.instance.transform;

        //sets hard limits for camera
        halfHeight = Camera.main.orthographicSize;
        halfWidth = halfHeight * Camera.main.aspect;
        
        bottomLeftLimit = myMap.localBounds.min + new Vector3(halfWidth, halfHeight, 0f);
        topRightLimit = myMap.localBounds.max - new Vector3(halfWidth, halfHeight, 0f);

        //sends those values to the player controller
        PlayerController.instance.SetBounds(myMap.localBounds.min,
                                            myMap.localBounds.max);

    }

    // LateUpdate is called after Update once per frame
    void LateUpdate()
    {
        if (!GameManager.instance.battleActive)
        {
            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

            //keeps camera inside bounds
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x),
                                            Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y),
                                            transform.position.z);

        }
        else
        {

        }
        //calls the music to start playing from the audio manager
        if(!musicStarted)
        {
            musicStarted = true;
            AudioManager.instance.PlayBGM(musicToPlay);
        }
    }
}
