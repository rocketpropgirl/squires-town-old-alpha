﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogManager : MonoBehaviour
{
    public TextMeshProUGUI dialogText;
    public TextMeshProUGUI nameText;
    public GameObject dialogBox;
    public GameObject nameBox;

    public string[] dialogLines;

    public int currentLine;
    private bool justStarted;

    public static DialogManager instance;
    private bool isTyping = false;
    private bool cancelTyping = false;

    public int typeSpeed;
    public int letter;
    public bool JustExitedDialog = false;

    private string questToMark;
    private bool markQuestComplete;
    private bool shouldMarkQuest;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;


    }

    // Update is called once per frame
    void Update()

    {
        //this controls text and dialog box advancement
        //also includes auto-typing text
        if (dialogBox.activeInHierarchy)
        {
            //advances the dialog one step when the player presses a button
            if (Input.GetButtonDown("Fire1") && dialogBox.activeInHierarchy)
            {
                justStarted = false;
                if (!isTyping)
                {
                    if (!justStarted)
                    {
                        currentLine++;

                        //when we run out of lines close the box!!
                        if (currentLine >= dialogLines.Length)
                        {
                            dialogBox.SetActive(false);
                            JustExitedDialog = true;
                            Invoke("SetBoolBack", 1f);

                            //let the player move again
                            GameManager.instance.dialogActive = false;
                            currentLine = 0;
                            //quest
                            if(shouldMarkQuest)
                            {
                                shouldMarkQuest = false;
                                if(markQuestComplete)
                                {
                                    QuestManager.instance.MarkQuestComplete(questToMark);
                                }
                                else
                                {
                                    QuestManager.instance.MarkQuestIncomplete(questToMark);

                                }
                            }

                        }
                        else
                        {
                            CheckIfName();
                            //display the new text
                            //in the text object
                            StartCoroutine(TextScroll(dialogLines[currentLine]));

                        }
                    }
                }
                else if (isTyping && !cancelTyping && !justStarted)
                {
                    justStarted = false;
                    cancelTyping = true;
                }
            }
        }
    }

    //scrolling text
    //this is what makes the text appear one letter
    //at a time into the dialog box
    private IEnumerator TextScroll(string lineOfText)
    {
    int letter = 0;
    dialogText.text = "";
    isTyping = true;
    cancelTyping = false;
    while (isTyping && !cancelTyping && letter < lineOfText.Length - 1) 
        {
        dialogText.text += lineOfText[letter];
        letter += 1;
        yield return new WaitForSeconds(typeSpeed);
        }
    dialogText.text = dialogLines[currentLine];
    isTyping = false;
    cancelTyping = false;

    }


    //an NPC needs to call the dialog features and
    //populate them with unique lines.
    public void ShowDialog(string[] newLines, bool isPerson)
    {
        currentLine = 0;
        justStarted = true;
        //set the current line back to 0 when we load a new dialog box, 
        //then import the lines from the npc.
        dialogLines = newLines;

        //name check MUST come after lines are imported
        CheckIfName();

        //displays the first line of dialog and sets
        //the needed bools to activate conversation
        dialogBox.SetActive(true);
        cancelTyping = false;
        StartCoroutine(TextScroll(dialogLines[currentLine]));


        nameBox.SetActive(isPerson);
        GameManager.instance.dialogActive = true;
    }

    public void CheckIfName()
    {
        if(currentLine >= dialogLines.Length)
        {
            dialogBox.SetActive(false);
            JustExitedDialog = true;
            Invoke("SetBoolBack", 1f);

            //let the player move again
            GameManager.instance.dialogActive = false;
        }
        //if its a name we need to put it in the Name Box
        else if (dialogLines[currentLine].StartsWith("n-"))
        {
            nameText.text = dialogLines[currentLine].Replace("n-","");

            currentLine++;

        }
    }
    private void SetBoolBack()
    {
        JustExitedDialog = false;
    }

    public void ShouldActivateQuestAtEnd(string questName, bool markComplete)
    {
        questToMark = questName;
        markQuestComplete = markComplete;
        shouldMarkQuest = true;
    }
}
