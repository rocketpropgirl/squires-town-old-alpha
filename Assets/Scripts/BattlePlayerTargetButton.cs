﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattlePlayerTargetButton : MonoBehaviour
{

    public string itemName;
    public int activeBattlerTarget;
    public TextMeshProUGUI playerName;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PressPlayerTarget()
    {
        BattleManager.instance.UseItemBattle(itemName, activeBattlerTarget);         

    }
}
