﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectEffect : MonoBehaviour
{
    public bool effectShow;
    public int effectLength;

    public static SelectEffect instance;


    // Start is called before the first frame update
    void Start()
    {
      effectShow = true;

    instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(!effectShow && gameObject.activeInHierarchy || !BattleManager.instance.battleScene.activeInHierarchy)
        {
            effectShow = false;
            Destroy(gameObject, effectLength);

        }
    }
}
