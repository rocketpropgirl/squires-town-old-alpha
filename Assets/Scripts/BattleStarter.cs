﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class BattleStarter : MonoBehaviour
{
    public BattleType[] potentialBattles;


    public bool activateOnEnter, activateOnStay, activateOnExit, activateOnExitConversation;

    public bool convoGO = false;

    private bool inArea;
    public float timeBetweenBattles;
    private float betweenBattleCounter;

    public bool deactiveAfterStarting;

    // Start is called before the first frame update
    void Start()
    {
        betweenBattleCounter = Random.Range(timeBetweenBattles * .5f, timeBetweenBattles * 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(inArea && PlayerController.instance.canMove)
        {
            if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                betweenBattleCounter -= Time.deltaTime;
            }

            if(betweenBattleCounter <= 0)
            {
                betweenBattleCounter = Random.Range(timeBetweenBattles * .5f, timeBetweenBattles * 1.5f);
                StartCoroutine(StartBattleCo());
            }
        }
        ConversationBattle();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if(activateOnEnter)
            {
                StartCoroutine(StartBattleCo());
            }
            else
            {
                inArea = true;

            }
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (activateOnExit)
            {
                StartCoroutine(StartBattleCo());
            }
            else
            {
                inArea = false;

            }
        }
    }

    public void ConversationBattle()
    {
        convoGO = DialogueLua.GetVariable("convoGO").asBool;
        if(activateOnExitConversation)
        {
            if(convoGO)
            {
                StartCoroutine(StartBattleCo());

            }

        }
    }

    public IEnumerator StartBattleCo()
    {
        UIFade.instance.FadeToBlack();
        GameManager.instance.battleActive = true;

        int selectedBattle = Random.Range(0, potentialBattles.Length);

        BattleManager.instance.rewardItems = potentialBattles[selectedBattle].rewardItems;
        BattleManager.instance.rewardXP = potentialBattles[selectedBattle].rewardXP;

        yield return new WaitForSeconds(2f);

        //pass enemies into
        BattleManager.instance.BattleStart(potentialBattles[selectedBattle].enemies);
        UIFade.instance.FadeFromBlack();

        if(deactiveAfterStarting)
        {
            gameObject.SetActive(false);
        }


    }


}
