﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EssentialsLoader : MonoBehaviour
{
    //Add any essential game objects to this
    //and they will be loaded in to the scene.
    public GameObject UIScreen;
    public GameObject player;
    public GameObject gameMan;
    public GameObject audioMan;
    public GameObject playerTarget;
    public GameObject battleMan;

    // Start is called before the first frame update
    void Awake()
    {
        //if we cant find the Fading mechanism, that means
        //the canvas got destroyed. make a new one.
        if(UIFade.instance == null)
        {
            Instantiate(UIScreen);
        }

        //if we cant find the player then
        //somehow the player got destroyed.
        //clone a new one
        if(PlayerController.instance == null)
        {
            LoadPlayer();
        }
        if(GameManager.instance == null)
        {
            Instantiate(gameMan);
        }
        if (AudioManager.instance == null)
        {
            Instantiate(audioMan);

        }

        if (BattleManager.instance == null)
        {
            Instantiate(battleMan);

        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadPlayer()
    {

        if (!PlayerPrefs.HasKey("Player_Position_x"))
        {
            PlayerController clone = Instantiate(player, new Vector3(playerTarget.transform.position.x,
                                                            playerTarget.transform.position.y,
                                                            playerTarget.transform.position.z), Quaternion.identity).GetComponent<PlayerController>();
            PlayerController.instance = clone;

        }
        else
        {
            PlayerController clone = Instantiate(player).GetComponent<PlayerController>();
            PlayerController.instance = clone;

        }

    }

}
