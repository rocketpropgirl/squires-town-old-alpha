﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string newGameScene;

    public GameObject continueButton;

    public string loadGameScene;


    // Start is called before the first frame update
    void Start()
    {
        //if there is save data in the quick save slot,
        //show the player the continue button
        //any key that you save can work, here i just used current scene which is in gamemenu
        if(PlayerPrefs.HasKey("Current_Scene"))
        {
            continueButton.SetActive(true);
        }
        else
        {
            continueButton.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Continue()
    {
        SceneManager.LoadScene(loadGameScene);

    }

    public void NewGame()
    {
        SceneManager.LoadScene(newGameScene);

    }

    public void Exit()
    {


    }
}
