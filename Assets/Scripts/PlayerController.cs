﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator myAnimator;
    public Rigidbody2D myRigidBody;
    public float moveSpeed;

    public static PlayerController instance;
    public string areaTransitionName;

    private Vector3 bottomLeftLimit;
    private Vector3 topRightLimit;

    public bool canMove = true; //canMove is tied to dialog and any other function that needs to stop the player

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;

        } else
        //THIS ALLOWS THE ESSENTIALS LOADER TO WORK
        //OTHERWISE THE PLAYER WILL DESTROY ITSELF
        //AFTER BEING LOADED
        {
            if(instance != this)
            {
                Destroy(gameObject);

            }
        }

        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (canMove)
        {
            //time to move!!
            //controls the velocity of the player in all directions.
            //tied to canMove and moveSpeed.
            myRigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), 
                                                Input.GetAxisRaw("Vertical")) * moveSpeed; 
        }
        else
        {
            //prevents movement on all axis
            //canMove must be false.
            myRigidBody.velocity = Vector2.zero;
        }
        myAnimator.SetFloat("moveX", myRigidBody.velocity.x);
        myAnimator.SetFloat("moveY", myRigidBody.velocity.y);

        //the player has told us they'd like to move
        //*controller input has been detected.*
        if (Input.GetAxisRaw("Horizontal") == 1 || 
            Input.GetAxisRaw("Horizontal") == -1 ||
            Input.GetAxisRaw("Vertical") == 1 ||
            Input.GetAxisRaw("Vertical") == -1)
        {
            //handles the animations for the player
            //only works if canMove
            if (canMove)
            {
                myAnimator.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
                myAnimator.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
            }
        }
        //clamps player to inside the map
        //dependant on calculations done at 
        //level start.
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x),
                                Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y),
                                transform.position.z);


    }
    public void SetBounds(Vector3 botLeft, Vector3 topRight)
    {
        bottomLeftLimit = botLeft + new Vector3 (.5f,.5f,0f);
        topRightLimit = topRight + new Vector3(-.5f, -.5f, 0f);

    }
}
