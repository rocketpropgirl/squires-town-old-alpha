﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    public GameObject myMenu;

    public GameObject[] windows;

    private CharStats[] playerStats;

    public TextMeshProUGUI[] nameText, hpText, mpText, lvlText, expText;
    public Slider[] expSlider;
    public Image[] charImage;
    public GameObject[] charStatHolder;

    public ItemButton[] itemButtons;
    public string selectedItem;
    public Item activeItem;
    public TextMeshProUGUI itemName, itemDescription, useButtonText;

    public static GameMenu instance;

    public GameObject useItemBox;
    public TextMeshProUGUI[] useItemBoxNames;

    public TextMeshProUGUI goldText;
    public bool isTyping;

    public string mainMenuName;

    // Start is called before the first frame update
    void Start()
    {

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2") && !GameManager.instance.shopActive && !GameManager.instance.battleActive)
        {
            if (myMenu.activeInHierarchy)
            {
                //myMenu.SetActive(false);
                //GameManager.instance.gameMenuOpen = false;

                CloseMenu();
            }
            else
            {
                myMenu.SetActive(true);
                UpdateMainStats();
                GameManager.instance.gameMenuOpen = true;

            }
            AudioManager.instance.PlaySFX(5);
        }

    }

    public void UpdateMainStats()
    {
        playerStats = GameManager.instance.playerStats;

        for (int i = 0; i < playerStats.Length; i++)
        {
            if (playerStats[i].gameObject.activeInHierarchy)
            {
                charStatHolder[i].SetActive(true);

                nameText[i].text = playerStats[i].charName;
                hpText[i].text = "HP: " + playerStats[i].currentHP + "/" + playerStats[i].maxHP;
                mpText[i].text = "MP: " + playerStats[i].currentMP + "/" + playerStats[i].maxMP;
                lvlText[i].text = "Lvl: " + playerStats[i].playerLevel;
                expText[i].text = "" + playerStats[i].currentEXP + "/" + playerStats[i].expToNextLevel[playerStats[i].playerLevel];

                expSlider[i].maxValue = playerStats[i].expToNextLevel[playerStats[i].playerLevel];
                expSlider[i].value = playerStats[i].currentEXP;
                charImage[i].sprite = playerStats[i].charImage;


            }
            else
            {
                charStatHolder[i].SetActive(false);
            }
        }

        goldText.text = GameManager.instance.currentGold.ToString() + "g";

    }

    public void ToggleWindow(int windowNumber)
    {
        for (int i = 0; i < windows.Length; i++)
        {
            if (i == windowNumber)
            {
                windows[i].SetActive(!windows[i].activeInHierarchy);
            }
            else
            {
                windows[i].SetActive(false);
            }
        }
        UpdateMainStats();
        useItemBox.SetActive(false);

        if(myMenu.activeInHierarchy)
        {
            itemButtons[0].Press();

        }
    }

    public void CloseMenu()
    {
        for (int i = 0; i < windows.Length; i++)
        {
            windows[i].SetActive(false);
        }

        myMenu.SetActive(false);

        GameManager.instance.gameMenuOpen = false;

        useItemBox.SetActive(false);

    }

    public void ShowItems()
    {
        GameManager.instance.SortItems();

        for (int i = 0; i < itemButtons.Length; i++)
        {
            itemButtons[i].buttonValue = i;

            if (GameManager.instance.itemsHeld[i] != "")
            {
                itemButtons[i].buttonImage.gameObject.SetActive(true);
                itemButtons[i].buttonImage.sprite = GameManager.instance.GetItemDetails(GameManager.instance.itemsHeld[i]).itemSprite;
                itemButtons[i].amountText.text = GameManager.instance.numberOfitems[i].ToString();
            }
            else
            {
                itemButtons[i].buttonImage.gameObject.SetActive(false);
                itemButtons[i].amountText.text = "";
            }
        }

    }

    public void SelectItem(Item newItem)
    {

        activeItem = newItem;

        if (activeItem.isItem)
        {
            useButtonText.text = "Use";
        }

        if (activeItem.isWeapon || activeItem.isArmor)
        {
            useButtonText.text = "Equip";
        }

        itemName.text = activeItem.itemName;
        StartCoroutine(TextScroll(activeItem.description));
    }

    private IEnumerator TextScroll(string lineOfText)
    {
        isTyping = true;
        int letter = 0;
        itemDescription.text = "";

        while (letter < lineOfText.Length - 1)
        {
            itemDescription.text += lineOfText[letter];
            letter += 1;
            yield return new WaitForSeconds(DialogManager.instance.typeSpeed);

        }
        isTyping = false;


    }

    public void DiscardItem()
    {
        try
        {
            GameManager.instance.RemoveItem(activeItem.itemName);

        }
        catch
        {
            Debug.LogError("Couldn't find the Item!");
            //what the fuck

        }

    }


    public void OpenUseItem()
    {

        useItemBox.SetActive(true);

        for(int i = 0; i < useItemBoxNames.Length; i++)
        {
            useItemBoxNames[i].text = GameManager.instance.playerStats[i].charName;
            useItemBoxNames[i].transform.parent.gameObject.SetActive(GameManager.instance.playerStats[i].gameObject.activeInHierarchy);
        }
    }

    public void CloseUseItem()

    {
        useItemBox.SetActive(false);

    }

    public void UseItem(int selectChar)
    {
        activeItem.Use(selectChar);
        CloseUseItem();
    }

    public void SaveGame()
    {
        GameManager.instance.SaveData();
        QuestManager.instance.SaveQuestData();
    }


    public void PlayButtonSound()
    {
        AudioManager.instance.PlaySFX(4);
    }

    public void QuitGame()
    {
        Destroy(GameObject.FindGameObjectWithTag("MainCamera"));
        SceneManager.LoadScene(mainMenuName);
        Destroy(GameManager.instance.gameObject);
        Destroy(PlayerController.instance.gameObject);
        Destroy(AudioManager.instance.gameObject);
        Destroy(gameObject);

    }
}
