﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemBattleButton : MonoBehaviour
{
    public string itemName;
    public int itemAmount;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI AmountText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Press()
    {
        BattleManager.instance.OpenPlayerTargetMenu(itemName);

    }
}
