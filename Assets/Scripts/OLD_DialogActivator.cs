﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogActivator : MonoBehaviour
{

    public string[] lines;


    private bool canActivate;

    public bool isPerson = true;

    public bool shouldActivateQuest;
    public string questToMark;
    public bool markComplete;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //the player talks to an NPC
        //the NPC passes their lines to the
        //dialog manager
        if(canActivate && Input.GetButtonUp("Fire1") &&
            !DialogManager.instance.dialogBox.activeInHierarchy &&
            !DialogManager.instance.JustExitedDialog)
        {
            DialogManager.instance.ShowDialog(lines, isPerson);
            DialogManager.instance.ShouldActivateQuestAtEnd(questToMark, markComplete);
        }
    }

    //if the player enters the trigger
    //collider, we want to set the bool
    //to true so that the npc CAN talk
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            canActivate = true;
        }
        
    }
    //if the player exits the trigger
    //collider, we want to set the bool
    //to false so that the npc CANT talk
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            canActivate = false;
        }

    }

}
