﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleReward : MonoBehaviour
{
    public static BattleReward instance;

    public TextMeshProUGUI xpText, itemText;
    public GameObject rewardsPanel;

    public string[] rewardItems;
    public int xpEarned;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Y))
        {
            OpenRewardsScreen(54, new string[] { "Iron Sword", "Iron Armor" });
        }
        
    }

    public void OpenRewardsScreen(int xp, string[]rewards)
    {
        xpEarned = xp;
        rewardItems = rewards;


        xpText.text = "Everyone earned " + xpEarned + " xp!";
        itemText.text = "";

        for(int i = 0; i < rewardItems.Length;i++)
        {
            //\n tells unity to use a new line.
            itemText.text += rewards[i] + "\n";

        }
        rewardsPanel.SetActive(true);

    }

    public void CloseRewardScreen()
    {
        for( int i = 0; i < GameManager.instance.playerStats.Length;i++)
        {
            if(GameManager.instance.playerStats[i].gameObject.activeInHierarchy)
            {
                GameManager.instance.playerStats[i].AddExp(xpEarned);
            }
        }

        for(int i = 0; i < rewardItems.Length; i++)
        {
            GameManager.instance.AddItem(rewardItems[i]);
        }
        UIFade.instance.FadeFromBlack();
        AudioManager.instance.PlayBGM(FindObjectOfType<CameraController>().musicToPlay);
        rewardsPanel.SetActive(false);
        GameManager.instance.battleActive = false;
    }
}
