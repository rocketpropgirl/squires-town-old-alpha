﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealNumber : MonoBehaviour
{
    public TextMeshProUGUI healText;

    public float lifetime = 1f;
    public float moveSpeed;

    public float placementJitter = .5f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, lifetime);
        transform.position += new Vector3(0f, moveSpeed * Time.deltaTime, 0f);
    }

    //sets the positive heal number to the correct ammount
    //and then displays that with the correct hp/mp/str text
    public void SetHeal(int healAmount, Item itemToUse)
    {
        if (itemToUse.affectHP)
        {
            healText.text = "" + "+" + healAmount.ToString() + "hp restored!";
            transform.position += new Vector3(Random.Range(-placementJitter, placementJitter),
                                            Random.Range(-placementJitter, placementJitter),
                                            0f);

        }
        if (itemToUse.affectMP)
        {
            healText.text = "" + "+" + healAmount.ToString() + "mp restored!";
            transform.position += new Vector3(Random.Range(-placementJitter, placementJitter),
                                            Random.Range(-placementJitter, placementJitter),
                                            0f);
        }

        if (itemToUse.affectStr)
        {
            healText.text = "" + "+" + healAmount.ToString() + "str bonus!";
            transform.position += new Vector3(Random.Range(-placementJitter, placementJitter),
                                            Random.Range(-placementJitter, placementJitter),
                                            0f);

        }
    }
}
