﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleChar : MonoBehaviour
{

    public bool isPlayer;
    public string[] movesAvailable;

    public string charName;
    public int currentHP, maxHP, currentMP, maxMP, strength, defence, wpnPower, armrPower;
    public bool hasDied;

    public SpriteRenderer mySprite;
    public Sprite deadSprite;
    public Sprite aliveSprite;

    private bool shouldFade;
    public float fadeSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(shouldFade)
        {
            mySprite.color = new Color(Mathf.MoveTowards(mySprite.color.r, 1f, fadeSpeed * Time.deltaTime), 
                                        Mathf.MoveTowards(mySprite.color.g, 0f, fadeSpeed * Time.deltaTime),
                                        Mathf.MoveTowards(mySprite.color.b, 0f, fadeSpeed * Time.deltaTime),
                                        Mathf.MoveTowards(mySprite.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (mySprite.color.a == 0)
            {
                gameObject.SetActive(false);

            }
        }
        
    }

    public void EnemyFade()
    {
        shouldFade = true;
    }
}
