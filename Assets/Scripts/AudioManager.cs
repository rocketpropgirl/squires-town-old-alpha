﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource[] sfx;
    public AudioSource[] bgm;

    public static AudioManager instance;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;

        }
        else
        //THIS ALLOWS THE ESSENTIALS LOADER TO WORK
        //OTHERWISE THIS OBJECT WILL DESTROY ITSELF
        //AFTER BEING LOADED
        {
            if (instance != this)
            {
                Destroy(gameObject);

            }
        }

        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
       
    }


    public void PlaySFX(int soundToPlay)
    {
        if(soundToPlay < sfx.Length)
        {
            sfx[soundToPlay].Play();

        }
    }

    public void PlayBGM(int musicToPlay)
    {
        //if the music isnt already playing
        if(!bgm[musicToPlay].isPlaying)
        {
            //play the music
            StopMusic();
            if (musicToPlay < bgm.Length)
            {
            bgm[musicToPlay].Play();

            }
        }
    }

    //stops all BGM tracks no matter what, as long as they are in the bgm[] array
    public void StopMusic()
    {
        for(int i = 0; i < bgm.Length; i++)
        {
            bgm[i].Stop();
        }
    }

}
